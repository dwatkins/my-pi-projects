/* Basic test of all GPIO outputs.
   Toggles all pins up and down for 400ms each
   cycling through all 17 GPIO pins 
   
   By Dave Watkins */

#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

int main (void)
{

  int i = 0;
  int j = 0;
  printf("Raspberry Pi GPIO test\n");

  if (wiringPiSetup() == -1)
    exit(1);

  for (j=0; j<17; j++){
    pinMode(j, OUTPUT);
    printf("Testing Pin %d\n", j);
    for (i=0; i<20; i++){
      delay(20);
      digitalWrite(j, !digitalRead(j));
    }
  }
  return 0;
}